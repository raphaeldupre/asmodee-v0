# Translation of Plugins - ACF Content Analysis for Yoast SEO - Stable (latest release) in French (France)
# This file is distributed under the same license as the Plugins - ACF Content Analysis for Yoast SEO - Stable (latest release) package.
msgid ""
msgstr ""
"PO-Revision-Date: 2019-11-16 09:35:22+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: GlotPress/3.0.0-alpha.2\n"
"Language: fr\n"
"Project-Id-Version: Plugins - ACF Content Analysis for Yoast SEO - Stable (latest release)\n"

#. translators: %1$s resolves to ACF Content Analysis for Yoast SEO, %2$s
#. resolves to Yoast SEO for WordPress, %3$s resolves to the minimal plugin
#. version
#: inc/dependencies/dependency-yoast-seo.php:67
msgid "%1$s requires %2$s %3$s or higher, please update the plugin."
msgstr "%1$s nécessite %2$s %3$s ou supérieur, veuillez mettre à jour l’extension."

#. Author URI of the plugin
msgid "http://angrycreative.se"
msgstr "http://angrycreative.se"

#. Author of the plugin
msgid "Thomas Kräftner, ViktorFroberg, marol87, pekz0r, angrycreative, Team Yoast"
msgstr "Thomas Kräftner, ViktorFroberg, marol87, pekz0r, angrycreative, équipe Yoast"

#. Plugin URI of the plugin
msgid "https://wordpress.org/plugins/acf-content-analysis-for-yoast-seo/"
msgstr "https://wordpress.org/plugins/acf-content-analysis-for-yoast-seo/"

#. Plugin Name of the plugin
msgid "ACF Content Analysis for Yoast SEO"
msgstr "ACF Content Analysis for Yoast SEO"

#. translators: %1$s resolves to ACF Content Analysis for Yoast SEO
#: yoast-acf-analysis.php:55
msgid "%1$s could not be loaded because of missing files."
msgstr "%1$s n’a pas pu être chargé à cause de fichiers manquants."

#. translators: %1$s resolves to ACF Content Analysis for Yoast SEO, %2$s
#. resolves to Yoast SEO for WordPress, %3$s resolves to the minimal plugin
#. version
#: inc/dependencies/dependency-yoast-seo.php:52
msgid "%1$s requires %2$s %3$s (or higher) to be installed and activated."
msgstr "%1$s nécessite que %2$s %3$s (ou supérieur) soit installé et activé."