<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 */

?>

<?php get_header(); ?>

	<section id="primary" class="content-area col-md-12">
		<div id="main" class="site-main" role="main">
			<div class="container">
				<div class="page404-wrapper">
					<div class="page404">
						<h1>404</h1>
						<p>La page demandée n'a pas pu être trouvée ...</p>
						<a href="/">Retour à l'accueil</a>
					</div>
				</div>
			</div>
		</div><!-- #main -->
	</section><!-- #primary -->


<?php get_footer(); ?>