<footer>
    <div class="container">
        <div class="footer-wrapper row">

            <!-- Footer gauche -->
            <div class="col-lg-3 footer-left">
                <?php
                    $nom_site = get_field('nom_du_site', 'option');
                    echo '<p>'. $nom_site .'</p>';
                ?>
            </div>

            <!-- Footer droit -->
            <div class="col-lg-9 footer-right">
                <?php
                    while( have_rows('mentions_legales', 'option') ) : the_row();
                        $titre = get_sub_field('titre');
                        $document = get_sub_field('document');
                        $document_url = $document['url'];

                        echo '<a href="'. esc_url($document_url) .'" target="_blank">'. $titre .'</a>';
                    endwhile;
                    while( have_rows('politique_de_protection_des_donnees_personnelles', 'option') ) : the_row();
                        $titre = get_sub_field('titre');
                        $document = get_sub_field('document');
                        $document_url = $document['url'];

                        echo '<a href="'. esc_url($document_url) .'" target="_blank">'. $titre .'</a>';
                    endwhile;
                ?>
            </div>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>

</body>
</html>