<?php

    // Chargement des styles et des scripts Bootstrap sur WordPress
    function asmodeeexperts_styles_scripts(){
        // Styles
        wp_enqueue_style('asmodeeexperts-owlcarousel-style', get_template_directory_uri() . '/lib/owlcarousel/owl.carousel.min.css');
        wp_enqueue_style('asmodeeexperts-owltheme-style', get_template_directory_uri() . '/lib/owlcarousel/owl.theme.default.min.css');
        wp_enqueue_style('asmodeeexperts-bootstrap-style', 'https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css');
        wp_enqueue_style('custom-style', get_stylesheet_uri());
        // Scripts
        wp_enqueue_script('asmodeeexperts-jquery', 'https://code.jquery.com/jquery-3.6.0.min.js');
        wp_enqueue_script('asmodeeexperts-jquery-ui', 'https://code.jquery.com/ui/1.12.1/jquery-ui.min.js');
        wp_enqueue_script('asmodeeexperts-owlcarousel', get_template_directory_uri() . '/lib/owlcarousel/owl.carousel.min.js');
        wp_enqueue_script('asmodeeexperts-bootstrap-popper', 'https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js');
        wp_enqueue_script('asmodeeexperts-bootstrap-script', 'https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js');
        wp_enqueue_script('custom-script', get_template_directory_uri() . '/js/main.js');
    }
    add_action('wp_enqueue_scripts', 'asmodeeexperts_styles_scripts');


    // Ajout de points d'accroche de menus
    function asmodeeexperts_after_setup_theme() {
        // On ajoute des menus
        register_nav_menu('menu_principal', "Menu principal");
        register_nav_menu('menu_footer_navigation', "Footer - Navigation");
        // On ajoute une classe php permettant de gérer les menus Bootstrap
        require_once get_template_directory() . '/lib/bootstrap/class-wp-bootstrap-navwalker.php';
    }
    add_action('after_setup_theme', 'asmodeeexperts_after_setup_theme');


    // Affiche la page Options de ACF Pro
    if( function_exists('acf_add_options_page') ) {
        acf_add_options_page();
    }


    // Ajout des images pour les actus
    add_theme_support( 'post-thumbnails' );


    // Taille d'image pour les univers
    add_image_size( 'univers', 392, 204);


    // Suppression accès page auteur
    function asmodeeexperts_disable_author_page() {
        global $wp_query;

        if ( is_author() ) {
            // Redirect to homepage
            wp_redirect(get_option('home'));
        }
    }
    add_action( 'template_redirect', 'asmodeeexperts_disable_author_page' );
    
?>