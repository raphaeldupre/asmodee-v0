<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php wp_title(); ?></title>
    <?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>

    <!-- Menu -->
    <nav class="navbar navbar-expand-lg navbar-light" role="navigation">
        <div class="container">

            <!-- Logo -->
            <a class="blog-header-logo text-dark" href="<?php bloginfo('url'); ?>">
                <?php
                    $logo = get_field('logo_menu', 'option');

                    echo '<img src="'. esc_url($logo['url']) .'" alt="'. esc_attr($logo['alt']) .'" class="img-fluid" />';
                ?>
            </a>

            <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header-menu" aria-controls="#header-menu" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button> -->

            <?php
                // wp_nav_menu(array(
                //     'theme_location'    => 'menu_principal',
                //     'depth'             => 2,
                //     'container'         => 'div',
                //     'container_class'   => 'collapse navbar-collapse',
                //     'container_id'      => 'header-menu',
                //     'menu_class'        => 'nav navbar-nav',
                //     'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                //     'walker'            => new WP_Bootstrap_Navwalker(),
                // ));
            ?>
            
        </div>
    </nav>

<?php wp_footer(); ?>