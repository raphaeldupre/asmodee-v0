<?php get_header(); ?>

	<section id="primary" class="content-area col-md-12">
		<div id="main" class="site-main" role="main">
			<div class="container">
				<div class="base-page-content pages-basiques row">

					<?php while ( have_posts() ) : the_post(); ?>
						<div class="page-title-wrapper col-md-12">
							<div class="page-title-content">
								<h1><?php the_title(); ?></h1>
							</div>
						</div>
						<div class="page-content-wrapper">
							<?php the_content(); ?>
						</div>
					<?php endwhile; ?>

				</div>
			</div>
		</div>
	</section>
	
<?php get_footer(); ?>