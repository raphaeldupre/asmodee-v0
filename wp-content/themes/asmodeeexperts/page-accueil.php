<?php get_header(); ?>

	<section id="primary" class="content-area">
		<div id="main" class="site-main" role="main">

			<!-- Top image -->
			<?php while ( have_posts() ) : the_post(); ?>
				<div class="home-top-wrapper">
					<div class="home-top-image">
						<?php the_post_thumbnail(); ?>
					</div>
					<div class="home-top-text">
						<?php the_content(); ?>
					</div>
				</div>
			<?php endwhile; ?>

			<!-- Liste des univers -->
			<div class="container">
				<div class="home-univers">
					<h2>Les univers</h2>
					<?php
						$args = array(
							'post_type'      => 'univers',
							'posts_per_page' => -1,
							'order' => 'ASC',
						);
						
						$query = new WP_Query( $args );

						if ( $query->have_posts() ) {
							echo '<div class="home-univers-wrapper row">';
								while ( $query->have_posts() ) { 
									$query->the_post();

									echo '<div class="home-univers-content-wrapper col-lg-4 col-md-6 col-sm-12">';
										echo '<div class="home-univers-content">';
											echo '<a href="'. get_permalink() .'">';
												echo the_post_thumbnail('univers');
											echo '</a>';
										echo '</div>';
									echo '</div>';
								}
								wp_reset_postdata();
							echo '</div>';
						}
					?>
				</div>
			</div>

		</div><!-- #main -->
	</section><!-- #primary -->

<?php get_footer(); ?>