<?php get_header(); ?>

	<section id="primary" class="content-area col-md-12">
		<div id="main" class="site-main" role="main">
			<div class="base-page-content-wrapper">
				<div class="container">

					<a href="/" class="asmodee-back">Retour</a>

					<?php while ( have_posts() ) : the_post(); ?>
						<div class="asmodee-univers row">

							<!-- Image Univers -->
							<div class="col-md-6 asmodee-univers-left">
								<?php the_post_thumbnail('univers'); ?>
							</div>

							<!-- Docs Univers -->
							<div class="col-md-6 asmodee-univers-right">
								<h1><?php the_title(); ?></h1>
								<p>Cliquez pour télécharger les éléments suivants :</p>
								<?php
									while( have_rows('documents') ) : the_row();
										$titre = get_sub_field('titre');
										$document = get_sub_field('document');
										$document_url = $document['url'];

										echo '<a href="'. esc_url($document_url) .'" target="_blank">'. $titre .'</a>';
									endwhile;
								?>
							</div>
						</div>
					<?php endwhile; ?>

				</div>
			</div>
		</div><!-- #main -->
	</section><!-- #primary -->
	
<?php get_footer(); ?>